import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import {Text, ListItem, Overlay, Input, Card} from 'react-native-elements';
import RekeningSection from './RekeningSection';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HistoryTransfer = () => {
  const [visible, setVisible] = React.useState(false);
  const [transactions, setTransactions] = React.useState([]);
  const [nama, setNama] = React.useState();
  const [noRekening, setNoRekening] = React.useState();
  const [saldo, setSaldo] = React.useState();
  const [rekeningPenerima, setRekeningPenerima] = React.useState();
  const [kodeBank, setKodeBank] = React.useState();
  const [amount, setAmount] = React.useState();
  const [berita, setBerita] = React.useState();

  React.useEffect(() => {
    pullData();
  }, []);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const postTransfer = () => {
    fetch('http://localhost:8080/transfer/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: await AsyncStorage.getItem('token'),
      },
      body: JSON.stringify({
        receiverNoRekening: rekeningPenerima,
        receiverVendorId: kodeBank,
        amount,
        berita,
      }),
    });
    window.location.reload();
  };

  const pullData = async () => {
    await fetch('http://localhost:8080/transaksi/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: await AsyncStorage.getItem('token'),
      },
      body: JSON.stringify({
        upperRange: '2020-10-31 04:38:04',
        lowerRange: '2020-10-31 04:38:04',
      }),
    }).then((resp) => {
      if (resp.status === 200) {
        resp.json().then((resp) => {
          setTransactions(resp.data);
        });
      } else if (resp.status === 401) {
        sessionStorage.removeItem('token');
        window.location.reload();
      }
    });
    await fetch('http://localhost:8080/rekening/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: await AsyncStorage.getItem('token'),
      },
      body: JSON.stringify({}),
    }).then((resp) => {
      if (resp.status === 200) {
        resp.json().then((resp) => {
          setNama(resp.fullname);
          setNoRekening(resp.noRekening);
          setSaldo(resp.saldo);
        });
      } else if (resp.status === 401) {
        sessionStorage.removeItem('token');
        window.location.reload();
      }
    });
  };

  return (
    <View>
      <RekeningSection
        noRekening={noRekening}
        saldo={saldo}
        nasabahName={nama}
      />
      <TouchableHighlight
        onPress={() => toggleOverlay()}
        style={{
          marginTop: 20,
          marginHorizontal: 50,
          alignItems: 'center',
          backgroundColor: '#2a89dc',
          padding: 10,
          borderRadius: 100,
        }}>
        <Text style={{color: 'white'}}>Transfer</Text>
      </TouchableHighlight>
      <Card>
        <Text>Recent Transactions</Text>
        {transactions.map((item, i) => (
          <ListItem key={i} bottomDivider onPress={() => toggleOverlay()}>
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </Card>
      <Overlay
        isVisible={visible}
        onBackdropPress={toggleOverlay}
        overlayStyle={{width: '90%', height: '55%'}}>
        <Input
          style={{paddingHorizontal: 10}}
          placeholder="No. Rekening Tujuan"
          onChange={(e) => setRekeningPenerima(e)}
          value={rekeningPenerima}
        />
        <Input
          style={{paddingHorizontal: 10}}
          placeholder="Kode Vendor"
          onChange={(e) => setKodeBank(e)}
          value={kodeBank}
        />
        <Input
          style={{paddingHorizontal: 10}}
          placeholder="Jumlah Transfer"
          onChange={(e) => setAmount}
          value={amount}
        />
        <Input
          style={{paddingHorizontal: 10}}
          placeholder="Berita"
          onChange={(e) => setBerita}
          value={berita}
        />
        <TouchableHighlight
          onPress={() => postTransfer()}
          style={{
            marginBottom: 20,
            marginHorizontal: 50,
            alignItems: 'center',
            backgroundColor: '#2a89dc',
            padding: 10,
            borderRadius: 100,
          }}>
          <Text style={{color: 'white'}}>Transfer</Text>
        </TouchableHighlight>
      </Overlay>
    </View>
  );
};

export default HistoryTransfer;
