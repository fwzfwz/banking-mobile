import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import {Input, Text} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
  const [userId, setUserId] = React.useState();
  const [pin, setPin] = React.useState();
  const [isLoading, setLoading] = React.useState(false);

  const doLogin = (e) => {
    setLoading(true);

    fetch('http://localhost:8080/login/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({userId: userId, pin: pin}),
    })
      .then((resp) => {
        if (resp.status == '202') {
          return resp.json();
        } else if (resp.status == '404') {
          alert('Wrong User ID or Pin');
        } else {
          alert('An error occured');
        }
      })
      .then((resp) => {
        console.log(resp);
        if (resp) {
          AsyncStorage.setItem('userId', userId);
          AsyncStorage.setItem('token', resp.token);
          navigation.navigate('HistoryTransfer');
        }
        setLoading(false);
      });
  };

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 25,
      }}>
      <Text
        h1
        style={{
          textAlign: 'center',
          marginBottom: 20,
        }}>
        BANK ABC
      </Text>
      <Input
        placeholder="UserID"
        value={userId}
        onChange={(value) => setUserId(value)}
      />
      <Input
        placeholder="Pin"
        secureTextEntry={true}
        value={pin}
        onChange={(value) => setPin(value)}
      />

      <TouchableHighlight
        onPress={() => doLogin()}
        style={{
          marginHorizontal: 50,
          alignItems: 'center',
          backgroundColor: '#2a89dc',
          padding: 10,
          borderRadius: 100,
        }}>
        <Text style={{color: 'white'}}>{isLoading ? '...' : 'Login'}</Text>
      </TouchableHighlight>
    </View>
  );
};

export default Login;
