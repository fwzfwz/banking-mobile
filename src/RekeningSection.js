import React from 'react';
import {View} from 'react-native';
import {Card, Text} from 'react-native-elements';

const RekeningSection = ({noRekening, saldo, nasabahName}) => {
  return (
    <Card style={styles.view}>
      <Text>{nasabahName}</Text>
      <View style={styles.rekeningView}>
        <Text>{noRekening}</Text>
        <Text>{saldo}</Text>
      </View>
    </Card>
  );
};

const styles = {
  view: {
    backgroundColor: '#42709e',
    height: '43%',
    paddingHorizontal: 25,
    paddingTop: 20,
    marginBottom: 20,
  },
  rekeningView: {
    marginTop: 15,
  },
  text: {
    color: 'whitesmoke',
  },
};

export default RekeningSection;
